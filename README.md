# Video Club

This project is an administrator for a video-club, using express.

## Getting Started

You should have a little of experience with docker, node and MySQL and MoongoDB.

Also you can use it with the deploy at back4app:

https://videoclubapptest-j18v3id7.b4a.run/   not uptdated because little troubles with the data base service btw

### Prerequisites

*NodeJS version 19 or higher.
*MySQL 5.7.33 or higher.
*Docker

### Installing

First clone the repo inside a desire folder.

```
git clone https://gitlab.com/a348503/video-club
```

Then install the dependencies

```
npm install
```

Also you must create an MySQL connection with the name video-club, password abcd1234 and user ROOt.
And inside, create an schema called video-clube. This if you want to use MySQL, also you can change 
this parameters inside de db.js file.


## Running the tests

If you didn't install the dependencies you should install: mocha, chai and supertest.

There are only tests for directors, you can run it by using the following command:

```
npm run test-directors
```

### Break down into end to end tests

This test try al the RESTful requests for the model directors.


## Deployment

Remember to create an schema named video-club if you are using MySQL.

## Built With

* [NodeJS](https://nodejs.org/es) - The execution enviroment.
* [Express](https://expressjs.com/) - The web framework.
* [MySQL](https://www.mysql.com/) - The Relational data base.

## Contributing

No contributions allowed.


## Authors

* **Gil C.** - *Student* - [Github](https://github.com/Gilelberto)


## License

Don't using just don't steal this without giving the references pls.

## Acknowledgments

* **Luis Antonio Ramírez Martínez** - *Teacher* 

